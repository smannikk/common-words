/*!
    Data structures and algorithms assignment, redone using C++ STL structures and algorithms
    instead of rolling my own.
*/

#include <iostream>
#include <unordered_map>
#include <string>
#include <algorithm>
#include <vector>
#include <utility>
#include <fstream>
#include <locale>

std::vector<std::pair<std::string, unsigned>> getCommonWords(std::string filename);
void countWordsFromFile(std::unordered_map<std::string, unsigned> &wordCount, std::string filename);

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        return 1;
    }

    std::string filename(argv[1]);
    std::vector<std::pair<std::string, unsigned>> results = getCommonWords(filename);

    for (size_t i=0; i<100 && i<results.size(); ++i)
    {
        std::cout << results[i].first << " " << results[i].second << "\n";
    }

    std::cin.get();
    return 0;
}

std::vector<std::pair<std::string, unsigned>> getCommonWords(std::string filename)
{
    std::unordered_map<std::string, unsigned> wordCount;
    countWordsFromFile(wordCount, filename);

    std::vector<std::pair<std::string, unsigned>> results;
    results.reserve(wordCount.size());
    for (const auto &item : wordCount)
    {
        results.push_back(item);
    }

    std::sort(results.begin(), results.end(),
        [](const std::pair<std::string, unsigned> &item1, const std::pair<std::string, unsigned> &item2)
        {
            return item1.second > item2.second;
        });

    return results;
}

void countWordsFromFile(std::unordered_map<std::string, unsigned> &wordCount, std::string filename)
{
    std::ifstream file(filename);
    if (!file.is_open())
    {
        return;
    }

    char ch;
    std::string word;
    while (file.get(ch))
    {
        if (std::isalpha(ch) || ch == '\'')
        {
            word += ch;
        }
        else if (!word.empty())
        {
            std::transform(word.begin(), word.end(), word.begin(), ::toupper);
            auto elem = wordCount.find(word);
            if (elem == wordCount.end())
            {
                wordCount[word] = 1;
            }
            else
            {
                ++elem->second;
            }
            word.clear();
        }
    }

    file.close();
}
